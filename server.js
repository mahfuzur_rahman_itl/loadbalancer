const { RateLimiterMemory } = require('rate-limiter-flexible')
const express = require('express')
const cors = require('cors')
const mongoose = require('mongoose')
const app = express()
const axios = require('axios')
const { v4: uuidv4 } = require('uuid')

mongoose.connect('mongodb://freeli:freeli267%23@182.163.122.135:57230/freeli_live_v3?authSource=freeli_live_v3')
const Logs = require('./models/logs')

// Application servers
const servers = [
    "http://localhost:4041/workfreeli",
    "http://localhost:4042/workfreeli"
]

// Track the current application server to send request 
let current = 0
let down_server = -1

app.use(cors())

// Enable JSON body parsing middleware
app.use(express.json())

// Rate limiting middleware
const rateLimiter = new RateLimiterMemory({
    points: 10, // 10 requests
    duration: 1, // per 1 second
});

app.use((req, res, next) => {
    req.requestId = uuidv4()
    req.time = Date.now()
    rateLimiter.consume(req.ip)
        .then(() => {
            next();
        })
        .catch(() => {
            let ld = new Logs({
                req_id: req.requestId,
                req_time: req.time,
                req_ip: req.ip,
                req_method: req.method,
                req_url: req.url,
                req_payload: req.headers['content-length'],
                res_status: '429',
                res_msg: 'Too Many Requests',
                res_time: Date.now(),
                res_server_address: '',
                res_payload: ''
            })
            ld.save()
            res.status(429).send('Too Many Requests');
        });
});


// Receive new request
// Forward to application server
const handler = async (req, res) => {
    console.log("Request payload size: ", req.headers["content-length"], "bytes")
    // Destructure following properties from request object
    const { method, url, headers, body } = req

    // Select the current server to forward the request
    let server = servers[current]
    // Loop until a successful response is received or all servers are tried
    while (true) {
        // Update track to select next server
        current === (servers.length - 1) ? current = 0 : current++
        try {
            // Requesting to underlying application server
            const response = await axios({
                url: `${server}`,
                method: method,
                headers: headers,
                data: body
            });
            // Send back the response data from application server to client 
            req.server_address = server
            // Log response payload size
            const responseSize = JSON.stringify(response.data).length;
            console.log("Response payload size: ", responseSize, "bytes");
            // Save log to database
            let ld = new Logs({
                req_id: req.requestId,
                req_time: req.time,
                req_ip: req.ip,
                req_method: req.method,
                req_url: req.url,
                req_operation_name: body.operationName,
                req_payload: req.headers['content-length'],
                res_status: response.status.toString(),
                res_msg: response.statusText,
                res_time: Date.now(),
                res_server_address: server,
                res_payload: responseSize
            });
            ld.save();
            // Send response back to client
            res.send(response.data);
            return; // Exit the loop if response is successful
        } catch (err) {
            console.error(`Error occurred while forwarding request to ${server}: ${err.message}`)
            server = servers[current]

            // If all servers have been tried, send a server error response
            if (servers.length === current + 1 && down_server !== -1) {
                down_server = -1
                res.status(500).send("All backend servers are down");
                return;
            }
            if (down_server === -1)
                down_server = current
        }
    }
}


function logResponsePayloadSize(req, res, next) {
    const originalSend = res.send;
    // Override the send method to capture response payload size
    res.send = function(body) {
        const responseSize = JSON.stringify(body).length
        console.log("Response payload size: ", responseSize, "bytes")
        let ld = new Logs({
            req_id: res.req.requestId,
            req_time: res.req.time,
            req_ip: res.req.ip,
            req_method: res.req.method,
            req_url: res.req.url,
            req_payload: res.req.headers['content-length'],
            res_status: '200',
            res_msg: 'Success',
            res_time: Date.now(),
            res_server_address: res.req.server_address,
            res_payload: responseSize
        })
        ld.save()
        originalSend.call(this, body)
    };

    next();
}
// app.use(logResponsePayloadSize)

// When receive new request
// Pass it to handler method
// app.use((req,res)=>{handler(req, res)});
app.post('/workfreeli', (req, res) => {
    handler(req, res)
});

// Listen on PORT 4001
app.listen(4001, err => {
    err ?
        console.log("Failed to listen on PORT 4001") :
        console.log("Load Balancer Server listening on PORT 4001");
});