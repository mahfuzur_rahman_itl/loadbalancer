var mongoose = require('mongoose')
var Schema = mongoose.Schema

var logsSchema = new Schema({
    req_id: { type: String, default: "" },
    req_time: { type: Date, default: Date.now },
    req_ip: { type: String, default: "" },
    req_method: { type: String, default: "" },
    req_url: { type: String, default: "" },
    req_operation_name: { type: String, default: "" },
    req_payload: { type: String, default: "" },
    res_status: { type: String, default: "" },
    res_msg: { type: String, default: "" },
    res_time: { type: Date },
    res_server_address: { type: String, default: "" },
    res_payload: { type: String, default: "" }
}, {
    timestamps: true
});

module.exports = mongoose.model('logs', logsSchema);